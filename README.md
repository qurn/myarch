# My Arch installation script

#-------- Prepare
loadkeys de-latin1
iwctl
station wlan0 scan
...
pacman -Sy git
git clone https://gitlab.com/qurn/myarch.git
cd myarch
bash run.sh

#if something fails
cryptsetup open /dev/sda2 cryptroot

mount /dev/mapper/cryptroot /mnt
mount /dev/sda1 /mnt/boot
arch-chroot /mnt
