#!/bin/bash

printf \
"
Enter username:
"
read USERNAME

printf \
"
Enter hostname:
"
read HOSTNAME

DRIVENAME_REPLACE
re='[0-9]'
if ! [[ ${DRIVE: -1} =~ $re ]] ; then
    PARTBOOT=$DRIVE\1
    PARTROOT=$DRIVE\2
else
    PARTBOOT=$DRIVE\p1
    PARTROOT=$DRIVE\p2
fi

printf \
"en_US ISO-8859-1
en_US.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE@euro ISO-8859-15" \
> /etc/locale.gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
locale-gen

printf \
"echo KEYMAP=de-latin1
echo FONT=lat9w-16" \
> /etc/vconsole.conf

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

echo $HOSTNAME > /etc/hostname
echo "127.0.0.1	$HOSTNAME.localdomain $HOSTNAME" >> /etc/hosts


echo "Which CPU?"
select cp in "AMD" "Intel"; do
    case $cp in
        AMD )
            cpu="AMD"
        break;;
        Intel )
            cpu="Intel"
        break;;
    esac
done


#-------- bootloader
echo "Which bootloader?"
select bs in "bootctl" "syslinux"; do
    case $bs in
        bootctl ) 
            pacman -Sy --noconfirm efibootmgr
            bootctl --esp-path=/boot install
            
            cp /etc/mkinitcpio.conf /etc/mkinitcpio.confBAK
            
            printf "timeout 0\ndefault arch" > /boot/loader/loader.conf
            
            CRYPTUUID="$(blkid -o value -s UUID $PARTROOT)"

	    
            printf \
            "title Archlinux
linux /vmlinuz-linux
" \
            > /boot/loader/entries/arch.conf

            if [ "$cpu" = "Intel" ]; then
                pacman -Sy --noconfirm intel-ucode
                printf \
                "initrd /intel-ucode.img
" \
                >> /boot/loader/entries/arch.conf
            fi
            if [ "$cpu" = "AMD" ]; then
                pacman -Sy --noconfirm amd-ucode
                printf \
                "initrd /amd-ucode.img
" \
                >> /boot/loader/entries/arch.conf
            fi
            printf \
            "initrd /initramfs-linux.img
options cryptdevice=UUID=$CRYPTUUID:cryptroot root=/dev/mapper/cryptroot quiet rw" \
            >> /boot/loader/entries/arch.conf

            break;;
        syslinux ) 
            pacman -Sy --noconfirm syslinux
            
            printf \
            "* BIOS: /boot/syslinux/syslinux.cfg
* UEFI: esp/EFI/syslinux/syslinux.cfg

PROMPT 0
TIMEOUT 50
DEFAULT arch

LABEL arch
	LINUX ../vmlinuz-linux
" \
            > /boot/syslinux/syslinux.cfg
            if [ "$cpu" = "Intel" ]; then
                pacman -Sy --noconfirm intel-ucode
                printf \
                "	INITRD ../intel-ucode.img,../initramfs-linux.img
" \
                >> /boot/syslinux/syslinux.cfg
            fi
            if [ "$cpu" = "AMD" ]; then
                pacman -Sy --noconfirm amd-ucode
                printf \
                "	INITRD ../amd-ucode.img,../initramfs-linux.img
" \
                >> /boot/syslinux/syslinux.cfg
            fi
            printf \
            "	APPEND root=$PARTROOT rw
	APPEND root=/dev/mapper/cryptroot cryptdevice=/dev/sda2:cryptroot" \
            >> /boot/syslinux/syslinux.cfg
            
            syslinux-install_update -iam
            break;;
    esac
done

#-------- add encrypt for syslinux and bootctl
printf \
"MODULES=()
BINARIES=()
FILES=()
HOOKS=(base udev autodetect modconf block filesystems keyboard encrypt lvm2 fsck)" \
> /etc/mkinitcpio.conf

mkinitcpio -p linux

#-------- users
printf \
"
Enter root pw:
"
passwd
printf \
"
Enter pw for user $USERNAME:
"
useradd -m -g users -G wheel,audio,video -s /bin/bash $USERNAME
passwd $USERNAME


mkdir /etc/systemd/system/getty@tty1.service.d
printf \
"[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin $USERNAME --noclear %%I \$TERM" \
> /etc/systemd/system/getty@tty1.service.d/override.conf


echo -e \
"%wheel ALL=(ALL) ALL\\n%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Syu,/usr/bin/pacman -Syyu,/usr/bin/packer -Syu,/usr/bin/packer -Syyu,/usr/bin/systemctl restart NetworkManager,/usr/bin/rc-service NetworkManager restart,/usr/bin/pacman -Syyu --noconfirm,/usr/bin/loadkeys,/usr/bin/yay,/usr/bin/pacman -Syyuw --noconfirm" \
>> /etc/sudoers

#-------- sound for slstatus
printf \
"snd-seq-oss
snd-pcm-oss
snd-mixer-oss" \
> /etc/modules-load.d/alsaoss.conf

sed -i "s/^#Color/Color/g" /etc/pacman.conf

#-------- git
cd /tmp
git clone https://gitlab.com/qurn/mydwm.git
cd mydwm
make clean install

cd ..
git clone https://gitlab.com/qurn/myslstatus.git
cd myslstatus
vim config.h
make clean install

cd ..
git clone https://gitlab.com/qurn/dotfiles.git
cd dotfiles
sudo -u $USERNAME bash move_files.sh

#-------- timesynchonisation
systemctl enable systemd-timesyncd.service

##-------- additional services
while true; do
    read -p $'Add big software? Y/N\n' yn
    case $yn in
        [Yy]* ) 
            pacman -Sy --needed --noconfirm adwaita-icon-theme \
                android-tools arduino cups eog fd firefox \
                gnome-disk-utility gnome-screenshot go gparted gzip hunspell-de \
                imagemagick kolourpaint libreoffice-fresh libreoffice-fresh-de \
                lxappearance mpv nemo nemo-fileroller newsboat octave okular \
                pavucontrol pidgin pidgin-otr poppler \
                poppler-data poppler-glib poppler-qt5 pkgfile \
                python python-matplotlib python-dbus python-dbus-common \
                python-pip qutebrowser ripgrep sane system-config-printer slock tor vlc \
                xfce4-appfinder xorg-xbacklight

            systemctl enable cups.service
            systemctl enable tor.service
            #gsettings set org.nemo.desktop show-desktop-icons false
            pkgfile -u
            printf "[Unit]\nDescription=Lock X session using slock for user %i\nBefore=sleep.target\n\n[Service]\nUser=%i\nEnvironment=DISPLAY=:0\nExecStartPre=/usr/bin/xset dpms force suspend\nExecStart=/usr/bin/slock\n\n[Install]\nWantedBy=sleep.target" > /etc/systemd/system/slock@.service
            systemctl enable slock@$USERNAME\.service

            #-------- aur helper
            cd /tmp
            sudo -u $USERNAME git clone https://aur.archlinux.org/paru-bin.git
            cd paru-bin
            sudo -u $USERNAME makepkg -sri --noconfirm
            sudo -u $USERNAME paru -S --noconfirm epson-inkjet-printer-escpr imagescan preload task-spooler urxvt-resize-font-git xbanish
            systemctl enable preload.service
            break;;
        [Nn]* ) 
            break;;
        * ) echo "Please answer yes or no.";;
    esac
done

#-------- grafic
lspci -k | grep -A 2 -E "(VGA|3D)"

echo "Which grafic card?"
select aind in "ati" "intel" "nvidia" "nvidia-470xx" "dont"; do
    case $aind in
        ati ) 
            pacman -Sy --needed --noconfirm mesa xf86-video-ati;
            break;;
        intel ) 
            pacman -Sy --needed --noconfirm mesa xf86-video-intel libva-intel-driver
            break;;
        nvidia ) 
            pacman -Sy --needed --noconfirm nvidia
            break;;
        nvidia-470xx ) 
            sudo -u $USERNAME paru -S --noconfirm nvidia-470xx-dkms
            break;;
        dont ) 
            break;;
    esac
done

echo "What kind of machine is this?"
select vlt in "virtualbox" "laptop" "tower" "wifi-tower" ; do
    case $vlt in
        virtualbox ) 
            pacman -Sy --needed --noconfirm virtualbox-guest-utils
            systemctl enable vboxservice.service
            systemctl enable dhcpcd.service
            systemctl enable vboxservice.service
            gpasswd -a $USERNAME vboxsf
            break;;
        laptop ) 
            systemctl enable dhcpcd.service
            pacman -Sy --needed --noconfirm iwd
            systemctl enable iwd.service
            break;;
        tower ) 
            systemctl enable dhcpcd.service
            break;;
        wifi-tower ) 
            systemctl enable dhcpcd.service
            pacman -Sy --needed --noconfirm iwd
            systemctl enable iwd.service
            break;;
    esac
done

while true; do
    read -p $'Rank mirrors? Y/N\n' yn
    case $yn in
        [Yy]* ) 
            pacman -Sy --noconfirm reflector
            reflector > /etc/pacman.d/mirrorlist
            break;;
        [Nn]* ) 
            break;;
        * ) echo "Please answer yes or no.";;
    esac
done

#
##microcode https://wiki.archlinux.org/index.php/Microcode
